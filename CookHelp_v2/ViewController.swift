//
//  ViewController.swift
//  CookHelp_v2
//
//  Created by Sławek Postawa on 16/06/2021.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var listRecipe = [RecipeData]()
    var searching = false;
    var searchedRecipe = [RecipeData]()
    
    let dataArray:[Ingredients] = Ingredients.generateArray()
    
    
    
  //  @IBOutlet weak var searchBarTest: UISearchBar!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(cameraTap))
        
      
        let recipe1 = RecipeData(recipeTitle: "Grzanki z serem i szynką", ingredientIds: [1,10,6,8,5], Img: "tost_pat")
        listRecipe.append(recipe1)
        
        
        let recipe2 = RecipeData(recipeTitle: "Pizza", ingredientIds: [1,11,5,6,9], Img: "pizza")
        listRecipe.append(recipe2)
        
        
        let recipe3 = RecipeData(recipeTitle: "Jajecznica", ingredientIds: [1,8,7], Img: "jajecznica")
        listRecipe.append(recipe3)
        
        
        let recipe4 = RecipeData(recipeTitle: "Neleśnik z pieczarkami i serem", ingredientIds: [1,5,6,11,4], Img: "nalesnik")
        listRecipe.append(recipe4)
        
        
        let recipe5 = RecipeData(recipeTitle: "Tortilla", ingredientIds: [2,3,4,6], Img: "tortilla")
        listRecipe.append(recipe5)
        
        let recipe6 = RecipeData(recipeTitle: "Tosty z serem i pieczarkami", ingredientIds: [10,8,5,6], Img: "tost")
        listRecipe.append(recipe6)
        
        let recipe7 = RecipeData(recipeTitle: "Tosty w jajku", ingredientIds: [1,10,6,9], Img: "tost2")
        listRecipe.append(recipe7)
        
        
        configureSerachController()
        
    }
    
    @objc func cameraTap() {
        let vc = storyboard?.instantiateViewController(identifier: "screen") as! PhotoViewController
        vc.modalPresentationStyle = .formSheet
        vc.completionHandler = { text in
            self.searchController.searchBar.text = text
        }
        present(vc, animated: true, completion: nil)
        
    }
    
        private func configureSerachController() {
        searchController.loadViewIfNeeded()
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.enablesReturnKeyAutomatically = false
        searchController.searchBar.returnKeyType = UIReturnKeyType.done
        self.navigationItem.searchController = searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
        searchController.searchBar.placeholder = "Masło, Jajka..."
    }


}



extension ViewController: UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return searchedRecipe.count
        }
        else {
            return listRecipe.count
        }
    }
    
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
       // cell.recipeImg.image = UIImage
        
        if searching {
            cell.recipeDes.attributedText = searchedRecipe[indexPath.row].recipeDes(searchText: searchController.searchBar.text ?? "")
            cell.recipeTitle.text = searchedRecipe[indexPath.row].recipeTitle
            cell.recipeImg.image = UIImage(named: searchedRecipe[indexPath.row].Img)
            cell.recipeImg.layer.cornerRadius = cell.recipeImg.frame.height / 2
            
        }
        else {
            cell.recipeDes.text = listRecipe[indexPath.row].recipeDes
            cell.recipeTitle.text = listRecipe[indexPath.row].recipeTitle
            cell.recipeImg.image = UIImage(named: listRecipe[indexPath.row].Img)
            cell.recipeImg.layer.cornerRadius = cell.recipeImg.frame.height / 2

            
        }
        return cell
        
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchedRecipe.removeAll()
        tableView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchText = searchController.searchBar.text!
        
        if !searchText.isEmpty {
            
            searching = true
            searchedRecipe.removeAll()
            for recipe in listRecipe {
                if searchText.lowercased().split(separator: " ").allSatisfy(recipe.recipeDes.lowercased().contains) {
                    searchedRecipe.append(recipe)
                }
                    
            }
            
        }
        else {
            searching = false
            searchedRecipe.removeAll()
            searchedRecipe = listRecipe
        }
        
        tableView.reloadData()
    }
    
}
