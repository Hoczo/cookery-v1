//
//  RecipeData.swift
//  CookHelp_v2
//
//  Created by Sławek Postawa on 16/06/2021.
//

import Foundation
import UIKit

struct RecipeData {
    
    var recipeTitle: String
    var ingredientIds: [Int]
    var Img: String
    
    var recipeDes : String {
        ingredientIds.map{ id in
            Ingredients.generateArray().first{$0.id == id}?.name ?? ""
        }.joined(separator: ", ")
    }
    func recipeDes(searchText: String) -> NSAttributedString {
        let ingredient = searchText.split(separator: " ").map{$0.lowercased()}
        return ingredientIds.map{ id in
            Ingredients.generateArray().first{$0.id == id}?.name ?? ""
        }.map{
            if ingredient.contains($0.lowercased()) {
            
            return NSAttributedString(string: $0)
            } else {
            return NSAttributedString(string: $0, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            }
        }.reduce(NSMutableAttributedString()) {(r: NSMutableAttributedString,e: NSAttributedString) -> NSMutableAttributedString in
            if !r.string.isEmpty {r.append(NSAttributedString (string: ", "))}
            r.append(e)
            return r
            
        }
    }
}
