//
//  TableViewCell.swift
//  CookHelp_v2
//
//  Created by Sławek Postawa on 16/06/2021.
//

import UIKit

class TableViewCell: UITableViewCell {
    

    @IBOutlet weak var recipeTitle: UILabel!
    
    @IBOutlet weak var recipeDes: UILabel!
    @IBOutlet weak var recipeImg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
