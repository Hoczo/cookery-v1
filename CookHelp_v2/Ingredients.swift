//
//  Ingredients.swift
//  CookHelp_v2
//
//  Created by Sławek Postawa on 29/06/2021.
//

import Foundation

struct Ingredients{
    
    var id:Int = 0
    var name:String = ""
    
   static func generateArray() -> [Ingredients] {
        var modelArray = [Ingredients]()
        
        
        modelArray.append(Ingredients(id: 1, name: "Jajka"))
        modelArray.append(Ingredients(id: 2, name: "Pomidory"))
        modelArray.append(Ingredients(id: 3, name: "Ogórki"))
        modelArray.append(Ingredients(id: 4, name: "Papryka"))
        modelArray.append(Ingredients(id: 5, name: "Pieczarki"))
        modelArray.append(Ingredients(id: 6, name: "Ser"))
        modelArray.append(Ingredients(id: 7, name: "Sól"))
        modelArray.append(Ingredients(id: 8, name: "Masło"))
        modelArray.append(Ingredients(id: 9, name: "Szynka"))
        modelArray.append(Ingredients(id: 10, name: "Czerstwy chelb"))
        modelArray.append(Ingredients(id: 11, name: "Mąka"))
        modelArray.append(Ingredients(id: 12, name: ""))
        modelArray.append(Ingredients(id: 13, name: ""))
    
    
        return modelArray
    }
}
