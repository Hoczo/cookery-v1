//
//  ViewController.swift
//  CookHelp
//
//  Created by Sławek Postawa on 14/06/2021.
//

import UIKit
import CoreML
import Vision

class PhotoViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
//    
//    required init?(coder: NSCoder) {
//        super.init(coder: coder)
//    }
    

   
   // @IBOutlet weak var serch: UISearchBar!
    var imagePicker: UIImagePickerController!
    
    
    @IBOutlet weak var des: UITextField!
 //   @IBOutlet weak var des2: UITextField!
    
    @IBOutlet weak var imageView: UIImageView!
    
 //   @IBOutlet weak var imageView2: UIImageView!
    
    
    var completionHandler: ((String?) -> Void)?
    
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        navigationItem.title = "Dodaj składniki"
    }


   
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageView.image=info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imagePicker.dismiss(animated: true, completion: nil)
        pictureIdentyfy(image: (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!)
        
        
    }
    
    
    func pictureIdentyfy(image:UIImage){
        
        guard let model = try? VNCoreMLModel(for: ModelVegetables_1().model) else {
            fatalError("Nie można załadować modelu")
        }
        let request = VNCoreMLRequest(model: model){
            [ weak self] request, Error in
            
            guard let results = request.results as? [VNClassificationObservation],
                  let firstResults = results.first else {
                fatalError("nie można uzyskać wyniku z VNCoreMLRequest")
            }
            
            DispatchQueue.main.async{
             //  self?.des.text = "confidence = \(Int(firstResults.confidence * 100))%"
                guard let self = self else{return}
                self.des.text = (self.des.text ?? "") + "\((firstResults.identifier)) "
                
            }
        }
        guard let ciImage = CIImage(image: image) else {
            fatalError("Nie można skonwertować zdjęcia")
            
        }
        let imageHandler = VNImageRequestHandler(ciImage: ciImage)
        
        DispatchQueue.global(qos: .userInteractive).async {
            do{
                try imageHandler.perform([request])
            }catch{
                print("Error \(error)")
            }
        }
            
    }
    
    @IBAction func photoButtom(_ sender: Any) {
        
        present(imagePicker, animated: true, completion: nil)
        
        
    }
    
    @IBAction func photoButton2(_ sender: Any) {
        
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func addButton(_ sender: Any) {
            
        completionHandler?(des.text)
        dismiss(animated: true)
    }

}
